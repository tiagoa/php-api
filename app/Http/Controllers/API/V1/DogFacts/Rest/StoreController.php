<?php

namespace App\Http\Controllers\API\V1\DogFacts\Rest;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\CatFacts\Rest\StoreRequest;
use App\Repositories\DogFactRepository;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class StoreController extends Controller
{
    protected DogFactRepository $repository;

    public function __construct(DogFactRepository $dogFactRepository)
    {
        $this->repository = $dogFactRepository;
    }

    public function __invoke(StoreRequest $request): Response
    {
        try {
            $catFact = $this->repository->store($request->all());
        } catch (Exception $exception) {
            abort(500, $exception->getMessage());
        }

        if (!$catFact) {
            abort(500, 'Dog fact could not be saved');
        }

        return response()->json($catFact);
    }
}
